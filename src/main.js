import Vue from 'vue'
import App from './App.vue'
import router from './router'

// import vuetron from 'vuetron'
// Vue.use(vuetron.VuetronVue)

import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)
import 'bootstrap-vue/dist/bootstrap-vue.css'

import Axios from 'axios'
Vue.prototype.$http = Axios.create({
  baseURL: '/api/'
})

import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
  color: '#6610f2'
})

import 'vue-awesome/icons/search'
import 'vue-awesome/icons/arrow-up'
import 'vue-awesome/icons/exclamation-circle'
import 'vue-awesome/icons/brands/twitter'
import 'vue-awesome/icons/brands/facebook'
import 'vue-awesome/icons/brands/weibo'
import 'vue-awesome/icons/brands/weixin'
import Icon from 'vue-awesome/components/Icon'
Vue.component('v-icon', Icon)

import SocialSharing from 'vue-social-sharing'
Vue.use(SocialSharing)

import VueQrcode from '@chenfengyuan/vue-qrcode'
Vue.component(VueQrcode.name, VueQrcode)


Vue.config.productionTip = false

Vue.prototype.$global = {}

Vue.filter('dateFormat', function (value) {
  if (!value) return ''
  let
    now = new Date(),
    past = new Date(value),
    daysPast = (now.getTime() - past.getTime()) / 1000 / 86400,
    isThisYear = past.getFullYear() == now.getFullYear()

  if (daysPast <= 1) return '今天'
  if (daysPast <= 2) return '昨天'
  if (daysPast <= 7) return `${parseInt(daysPast)} 天前`
  if (daysPast > 7) {
    if (isThisYear) return `${past.getMonth()} 月 ${past.getDate()} 日`
    else return `${past.getFullYear()} 年 ${past.getMonth()} 月 ${past.getDate()} 日`
  }
  return ''
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
