module.exports = {
  transpileDependencies: [
    /\bvue-awesome\b/
  ],
  chainWebpack: config => {
    const oneOfsMap = config.module.rule('scss').oneOfs.store
    oneOfsMap.forEach(item => {
      item
        .use('sass-resources-loader')
        .loader('sass-resources-loader')
        .options({
          // Provide path to the file with resources
          resources: './src/assets/_var.scss'
        })
        .end()
    })
  },
  devServer: {
    proxy: 'http://www.zuibishe.com',
    disableHostCheck: true
  },
  productionSourceMap: false
}